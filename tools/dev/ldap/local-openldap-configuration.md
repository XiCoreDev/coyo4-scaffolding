# Local OpenLDAP configuration

## Settings

### Verbindung

* Hostname: localhost
* Port: 389
* SSL: nein
* AD Domäne: mindsmash.com
* BasisDN: ou=company,dc=coyoapp,dc=com
* Benutzername: cn=admin,dc=coyoapp,dc=com
* Password: ldap

### Benutzer

* Zus.BenutzerDN: ou=people
* Klasse d. Benutzer-Object: person
* Filter des Benutzer-Objects: <leer>
* Benutzer-ID: entryUUID
* Benutzername: uid
* Vorname: givenName
* Nachname: sn
* Name: cn
* E-Mail: mail
* Profil-Felder: (alle optional)
    * phone : telephoneNumber
    * department : employeeType
    * website : uid

### Gruppen (optional)

* Zus.GruppenDN: ou=groups
* Klasse d. Gruppen-Object: groupofnames
* Filter des Benutzer-Objects: <leer>
* Gruppen-ID: distinguishedname
* Gruppen-Name: cn
* User Attr. für Mitgliedschaft: businessCategory
* Nested Groups: false

## HINTS

* in der Datei `tools/dev/ldap/default-data.ldif` befinden sich die Setup-Daten für den LDAP-Container. Dort sind die entsprechenden Nutzerdaten für die Accounts hinterlegt.
