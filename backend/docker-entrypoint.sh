#!/bin/bash

set -e;

/coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-db:5432"
/coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-es:9300"
/coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-mq:5672"
/coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-redis:6379"
/coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-elk:4560"
/coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-mongo:27017"

bash -C '/coyo/start.sh';'bash'
