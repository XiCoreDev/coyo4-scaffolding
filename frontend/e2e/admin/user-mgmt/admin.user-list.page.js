(function () {
  'use strict';

  function statusFilter(option) {
    var toggle = $('.fb-filter-toggle i');
    toggle.click();
    element(by.cssContainingText('.filter a', option)).click();
    toggle.click();
  }

  var repeater = 'row in vm.page.content track by row.id';

  module.exports = {
    get: function () {
      browser.get('/admin/user-management/users');
      require('../../testhelper.js').disableAnimations();
    },
    nameFilter: element(by.model('$ctrl.searchTerm')),
    statusFilter: {
      showAll: statusFilter.bind(undefined, 'Show All'),
      showActive: statusFilter.bind(undefined, 'Show Active'),
      showInactive: statusFilter.bind(undefined, 'Show Inactive'),
      showDeleted: statusFilter.bind(undefined, 'Show Deleted')
    },
    userTotal: $('coyo-counter span.hidden-xs'),
    createButton: $('.fb-actions-inline a[ui-sref="admin.user-management.user.create"]'),
    table: {
      headers: {
        name: element(by.cssContainingText('th', 'Name')),
        email: element(by.cssContainingText('th', 'Email'))
      },
      rows: {
        get: function (index) {
          var row = $$('tr[ng-repeat="' + repeater + '"]').get(index);
          return {
            row: row,
            name: row.$$('td').get(0),
            email: row.$$('td').get(1),
            groups: row.$$('td').get(2),
            role: row.$$('td').get(3),
            directory: row.$$('td').get(4),
            status: row.$$('td').get(5),
            options: function () {
              var el = row.$$('td').get(6);
              return {
                open: function () {
                  browser.actions().mouseMove(el).perform();
                  el.$('.dropdown-toggle').click();
                  browser.sleep(200); // otherwise unstable on bamboo
                },
                deleteOption: el.$('span[translate="ADMIN.USER_MGMT.USERS.OPTIONS.DELETE.MENU"]'),
                recoverOption: el.$('span[translate="ADMIN.USER_MGMT.USERS.OPTIONS.RECOVER.MENU"]'),
                activateOption: el.$('span[translate="ADMIN.USER_MGMT.USERS.OPTIONS.ACTIVATE.MENU"]'),
                deactivateOption: el.$('span[translate="ADMIN.USER_MGMT.USERS.OPTIONS.DEACTIVATE.MENU"]'),
                editOption: el.$('span[translate="ADMIN.USER_MGMT.USERS.OPTIONS.EDIT.MENU"]')
              };
            }
          };
        },
        names: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return row.$$('td').get(0).getText().then(function (name) {
              var lines = name.split('\n');
              return lines.length > 0 ? lines[0] : name;
            });
          });
        },
        emails: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return row.$$('td').get(1).getText();
          });
        },
        data: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return {
              name: row.$$('td').get(0).getText(),
              email: row.$$('td').get(1).getText(),
              groups: row.$$('td').get(2).getText(),
              role: row.$$('td').get(3).getText(),
              directory: row.$$('td').get(4).getText(),
              status: row.$$('td').get(5).getText()
            };
          });
        },
        count: function () {
          return element.all(by.repeater('' + repeater + '')).count();
        }
      },
      pagination: {
        links: element.all(by.css('.pagination-page')),
        first: $('.pagination-first'),
        prev: $('.pagination-prev'),
        next: $('.pagination-next'),
        last: $('.pagination-last')
      }
    }
  };

})();
