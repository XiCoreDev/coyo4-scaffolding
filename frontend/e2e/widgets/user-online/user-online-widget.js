(function () {
  'use strict';

  var login = require('../../login.page.js');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page.js');
  var UserOnlineWidget = require('./user-online-widget.page.js');
  var testhelper = require('../../testhelper');
  var components = require('../../components.page');

  describe('user-online widget', function () {

    var widgetSlot, widgetChooser, widget, userOnlineWidget, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      navigation = new Navigation();
      navigation.editView();
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      userOnlineWidget = new UserOnlineWidget(widget);

      widgetSlot.addButton.click();
      widgetChooser.selectByName('Online Users');
    });

    it('should be created', function () {
      // widgetChooser.saveButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(userOnlineWidget.renderedWidget.onlineUsersCount.getText()).toMatch(/\d+/);

      // remove widget
      navigation.editView();
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });
})();
