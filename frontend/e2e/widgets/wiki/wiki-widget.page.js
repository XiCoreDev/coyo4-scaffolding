(function () {
  'use strict';

  var extend = require('util')._extend;
  var Select = require('../../select.page');

  function WikiWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      wikiApp: new Select($('coyo-select-wiki-app')),
      articleCount: element(by.model('$ctrl.model.settings._articleCount'))
    };

    api.allArticles = element.all(by.css('[ng-repeat="article in ::$ctrl.articles"]'));
  }
  module.exports = WikiWidget;
})();
