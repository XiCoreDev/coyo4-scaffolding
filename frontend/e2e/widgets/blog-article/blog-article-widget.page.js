(function () {
  'use strict';

  var extend = require('util')._extend;
  var Select = require('../../select.page');

  function BlogArticleWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      blogArticle: new Select($('coyo-select-blog-article'))
    };

    api.renderedWidget = {
      articleSender: element(by.css('.article-sender')),
      articleTitle: element(by.css('.article-title')),
      teaserText: element(by.css('.teaser-text'))
    };
  }

  module.exports = BlogArticleWidget;
})();
