(function () {
  'use strict';

  var Select = require('../select.page');

  function PageCreate() {
    var api = this;

    api.page1 = {
      name: element(by.model('$ctrl.page.name')),
      nameTaken: $('p[ng-message="pageName"]'),
      category: new Select(element(by.model('$ctrl.page.categories')), true),
      continueButton: $('form[name="pageForm1"]').element(by.cssContainingText('button[type="submit"]', 'Continue')),
      cancelButton: element(by.cssContainingText('a[ui-sref="main.pages"]', 'Create'))
    };

    var form = $('form[name="pageForm2"]');
    api.page2 = {
      continueButton: form.element(by.cssContainingText('button[type="submit"]', 'Continue')),
      backButton: form.element(by.cssContainingText('a', 'Back'))
    };

    api.page3 = {
      createButton: element(by.cssContainingText('button[type="submit"]', 'Create')),
      backButton: $('form[name="pageForm3"]').element(by.cssContainingText('a', 'Back'))
    };
  }

  module.exports = PageCreate;

})();
